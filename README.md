# Wine.MS

## Description

Wine.MS is a wine recommender system built for a class project on data integration. The goal is to recommend wines available at local sellers in Münster based on flavor similarity to other wines the user inputs.
It consists of multiple scrapers to collect data on wines, a React.js front-end served by webpack, a Django API, and a PostgreSQL database. Additionally, this repository contains all code and data relevant to the required data processing steps.

## Instructions

Instructions to run each part of the project are located in each directory.
